<?php

return [

    //home e navbar

    'welcome' => 'Welcome to Presto',
    'crea un annuncio' => 'Create an ad',
    'categorie' => 'Categories',
    'visualizza annunci' => 'View ads',
    'ciao' => 'Hello',
    'ricerca' => 'Search',
    'registrati' => 'Register',
    'area revisore' => 'Revisor Home',
    'cestino' => 'Trash',
    'message'=>'Access not allowed - for reviewers only',
    'scopri'=>'Learn about us',
    'pubblicità'=>' Euro Coupon 10%: discount up to € 50.',
    'sconto'=>'Use the code and save',
    'secure'=>'Safer and more secure',
    'secure1'=>'To help protect you from online fraud, we use strong encryption methods.',
    'spedizioni'=>'Flash Shipments',
    'spedizioni1'=>'Fast delivery in 2 working days',
    'customer'=>'Customer Testimonials',
    'customer1'=>'because they love our services',
    'gli ultimi annunci'=>'The latest announcements',

    //crea annuncio

    'crea il tuo annuncio' => 'Create your ad',
    'categoria' => 'Category',
    'titolo annuncio' => 'Title',
    'inserisci il titolo' => 'Insert the title',
    'descrizione' => 'Description',
    'inserisci la descrizione' => 'Insert the description',
    'prezzo' => 'Price',
    'inserisci il prezzo' => 'Insert the price',
    'immagini' => 'Images',
    'trascina qui' => 'Drop here',
    'invia' => 'Send',

    //tutti gli annunci

    'tutti gli annunci' => 'All the ads',
    'vai al dettaglio' => 'Go to the datail',

    //home revisore

    'annuncio' => 'Ad',
    'utente' => 'User',
    'titolo' => 'Title',
    'descrizione' => 'Description',
    'immagine' => 'Image',
    'rifiuta' => 'Reject',
    'accetta' => 'Accept',
    'non ci sono articoli' => 'No article to review',

    //cestino

    'annunci cancellati' => 'Deleted ads',
    'ripristina' => 'Restore',
    'elimina definitivamente' => 'Permanently delete',

    //login

    'effettua il login' => 'Login',
    'e-mail' => 'E-mail addresse',
    'password' => 'Password',
    'ricordami' => 'Remember me',
    'password dimenticata' => 'Forgot your password?',

    //registrati

    'registrati' => 'Register',
    'conferma password' => 'Confirm Password',
    'nome' => 'Name',

    //dettaglio annncio

    'dettaglio annuncio' => 'Ad detail',
    'modifica' => 'Edit',
    'elimina' => 'Delete',

    //modifica annuncio

    'modifica il tuo annuncio' => 'Edit your ad',

    //footer

    'contattaci'=>'Get in touch',
    'aggiornamento'=>'Don’t miss any updates of our new templates and extensions.',
    'sottoscrivi'=>'Subscribe',
    'mail'=>'Send mail',
    'scarica'=>'Download',
    'azienda'=>'Company',
    'android'=>'Android App',
    'ios'=>'Ios App',
    'desktop'=>'Desktop',
    'progetti'=>'Projects',
    'compiti'=>'My Tasks',
    'aiuto'=>'Help',
    'faq'=>'Faq',
    'termini'=>'Terms',
    'condizioni'=>'Conditions',
    'segnalazioni'=>'Reportings',
    'documentazione'=>'Documentation',
    'supporto'=>'Support Policy',
    'privacy'=>'Privacy',
    'soluzioni'=>'Social',
    'diritti'=>'All rights reserved'






];
