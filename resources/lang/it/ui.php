<?php

return [

    //home e navbar

    'welcome' => 'Benvenuto in Presto',
    'crea un annuncio' => 'Crea un annuncio',
    'categorie' => 'Categorie',
    'visualizza annunci' => 'Visualizza annunci',
    'ciao' => 'Ciao',
    'ricerca' => 'Ricerca',
    'registrati' => 'Registrati',
    'area revisore' => 'Area revisore',
    'cestino' => 'Cestino',
    'message'=>' Accesso non consentito - solo per revisori',
    'scopri'=>'Su di noi',
    'pubblicità'=>'Euro Coupon 10%: sconto fino a 50€.',
    'sconto'=>'Usa il codice e risparmia',
    'secure'=>'Più sicuro e protetto',
    'secure1'=>'Per aiutare a proteggerti dalle frodi online, utilizziamo metodi di crittografia avanzata.',
    'spedizioni'=>'Spedizioni flash',
    'spedizioni1'=>'Consegnia rapida in 2 girni lavorativi',
    'customer'=>'Testimonianze dei clienti',
    'customer1'=>'e perche amano i nostri servizi',
    'gli ultimi annunci'=>'Gli ultimi annunci',
    //crea annuncio

    'crea il tuo annuncio' => 'Crea il tuo annuncio',
    'categoria' => 'Categoria',
    'titolo annuncio' => 'Titolo annuncio',
    'inserisci il titolo' => 'Inserisci il titolo',
    'descrizione' => 'Descrizione',
    'inserisci la descrizione' => 'Inserisci la descrizione',
    'prezzo' => 'Prezzo',
    'inserisci il prezzo' => 'Inserisci il prezzo',
    'immagini' => 'Immagini',
    'trascina qui' => 'Trascina qui',
    'invia' => 'Invia',

    //tutti gli annunci

    'tutti gli annunci' => 'Tutti gli annunci',
    'vai al dettaglio' => 'Vai al dettaglio',

    //home revisore

    'annuncio' => 'Annuncio',
    'utente' => 'Utente',
    'titolo' => 'Titolo',
    'descrizione' => 'Descrizione',
    'immagine' => 'Immagine',
    'rifiuta' => 'Rifiuta',
    'accetta' => 'Accetta',
    'non ci sono articoli' => 'Non ci sono articoli da revisionare',

    //cestino

    'annunci cancellati' => 'Annunci cancellati',
    'ripristina' => 'Ripristina',
    'elimina definitivamente' => 'Elimina definitivamente',

    //login

    'effettua il login' => 'Effettua il Login',
    'e-mail' => 'Indirizzo E-mail',
    'password' => 'Password',
    'ricordami' => 'Ricordami',
    'password dimenticata' => 'Password dimenticata?',

    //registrati

    'registati' => 'Registrati',
    'conferma password' => 'Conferma Password',
    'nome' => 'Nome',

    //dettaglio annncio

    'dettaglio annuncio' => 'Dettaglio annuncio',
    'modifica' => 'Modifica',
    'elimina' => 'Elimina',

    //modifica annuncio

    'modifica il tuo annuncio' => 'Modifica il tuo annuncio',

    //footer
    'contattaci'=>'Contattaci',
    'aggiornamento'=>'Non perdere nessun aggiornamento dei nostri nuovi modelli ed estensioni.',
    'sottoscrivi'=>'Iscriviti',
    'mail'=>'Invia mail',
    'scarica'=>'Download',
    'azienda'=>'Azienda',
    'android'=>'Android App',
    'ios'=>'Ios App',
    'desktop'=>'Desktop',
    'progetti'=>'Progetti',
    'compiti'=>'I miei compiti',
    'aiuto'=>'Aiuto',
    'faq'=>'Faq',
    'termini'=>'Termini',
    'condizioni'=>'Condizioni',
    'segnalazioni'=>'Segnalazioni',
    'documentazione'=>'Documentazioni',
    'supporto'=>'Politica di supporto',
    'privacy'=>'Privacy',
    'soluzioni'=>'Social',
    'diritti'=>'Tutti i diritti riservati'




];
