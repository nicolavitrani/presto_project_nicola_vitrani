<?php

return [

    //home e navbar

    'welcome' => 'Bienvenido en Presto',
    'crea un annuncio' => 'Crear un anuncio',
    'categorie' => 'Categorias',
    'visualizza annunci' => 'Ven anuncios',
    'ciao' => 'Hola',
    'ricerca' => 'Busca',
    'registrati' => 'Registrarse',
    'area revisore' => 'Area de revisor',
    'cestino' => 'Basura',
    'message'=>' Acceso no permitido: solo para revisores',
    'scopri'=>'Aprende sobre nosotros',
    'pubblicità'=>'Coupon Euro 10%: descuento hasta 50€.',
    'sconto'=>'Usa el código y ahorra',
    'secure'=>'Mas seguro y mas protegido',
    'secure1'=>'Para ayudarlo a protegerse del fraude en línea, utilizamos métodos de cifrado sólidos.',
    'spedizioni'=>'Envíos flash',
    'spedizioni1'=>'Entrega rápida en 2 días laborables',
    'customer'=>'Recomendaciones de clientes',
    'customer1'=>'porque les encantan nuestros servicios',
    'gli ultimi annunci'=>'Los últimos anuncios',

    //crea annuncio

    'crea il tuo annuncio' => 'Crea tu anuncio',
    'categoria' => 'Categoria',
    'titolo annuncio' => 'Título',
    'inserisci il titolo' => 'Ingrese el título',
    'descrizione' => 'Descripción',
    'inserisci la descrizione' => 'Ingrese la descripción',
    'prezzo' => 'Precio',
    'inserisci il prezzo' => 'Ingrese el precio',
    'immagini' => 'Imagenes',
    'trascina qui' => 'Arrastre aquí',
    'invia' => 'Enviar',

    //tutti gli annunci

    'tutti gli annunci' => 'Todos los anuncios',
    'vai al dettaglio' => 'Ir al detalle',

    //home revisore

    'annuncio' => 'Anuncio',
    'utente' => 'Usuario',
    'titolo' => 'Titulo',
    'descrizione' => 'Descripciòn',
    'immagine' => 'Imagen',
    'rifiuta' => 'Niega',
    'accetta' => 'Acepta',
    'non ci sono articoli' => 'No hay anuncios para revisar',

    //cestino

    'annunci cancellati' => 'Anuncios eliminados',
    'ripristina' => 'Restaurar',
    'elimina definitivamente' => 'Borrar permanentemente',

    //login

    'effettua il login' => 'Login',
    'e-mail' => 'E-mail',
    'password' => 'Password',
    'ricordami' => 'Recuerdame',
    'password dimenticata' => 'Password olvidada?',

    //registrati

    'registati' => 'Registrate',
    'conferma password' => 'Confirma Password',
    'nome' => 'Nombre',

    //dettaglio annncio

    'dettaglio annuncio' => "Detalle del anuncio",
    'modifica' => 'Cambiar',
    'elimina' => 'Retirar',

    //modifica annuncio

    'modifica il tuo annuncio' => 'Editar su anuncio',

   //footer
   'contattaci'=>'Contáctenos',
   'aggiornamento'=>'No te pierdas ninguna actualización sobre nuestros nuevos modelos y extensiones.',
   'sottoscrivi'=>'Suscribir',
   'mail'=>'Enviar correo',
   'scarica'=>'Descargar',
   'azienda'=>'Empresa',
   'android'=>'Android Aplicación',
   'ios'=>'Ios Aplicación',
   'desktop'=>'Escritorio',
   'progetti'=>'Proyectos',
   'compiti'=>'Mi tarea',
   'aiuto'=>'Ayudar',
   'faq'=>'Faq',
   'termini'=>'Condiciones',
   'condizioni'=>'Condiciones',
   'segnalazioni'=>'Informes',
   'documentazione'=>'Documentaciones',
   'supporto'=>'Política de soporte',
   'privacy'=>'Privacy',
   'soluzioni'=>'Social',
   'diritti'=>'reservados todos los derechos'


];
