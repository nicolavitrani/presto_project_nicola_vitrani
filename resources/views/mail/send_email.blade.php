<x-layout>

@if (session('message'))
    <div class="alert alert-success">
        Email inviata correttamente.
    </div>
@endif

    <div class="container-fluid">
        <div class="row">
            <div class="col-md-8">
                <form method="POST" action="{{route('contacts.submit')}}">
                    @csrf
                    <div class="mb-3">
                        <label for="exampleInputEmail1" class="form-label">Indirizzo Email</label>
                        <input type="email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" name="email">
                    </div>
                    <div class="mb-3">
                        <label for="exampleInputPassword1" class="form-label">Nome e cognome</label>
                        <input type="text" class="form-control" id="exampleInputPassword1" name="user">
                    </div>
                    <div class="mb-3 form-check">
                        <textarea name="message" cols="30" rows="10" placeholder="Inserisci un messaggio"></textarea>
                    </div>
                        <button type="submit" class="btn btn-primary">Submit</button>
                    </form>
            </div>
        </div>
    </div>
</x-layout>
