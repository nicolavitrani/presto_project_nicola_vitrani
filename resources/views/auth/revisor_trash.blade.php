<x-layout>
    <div class="container mt-4">
        <div class="row">
            <div class="col-md-12 text-center">
                <h1 class="my-2" > {{ __('ui.annunci cancellati')}} </h1>
            </div>
        </div>
    </div>
    <div class="container mt-4 mb-4">
        <div class="row">
            <div class="col-12">
                <h6 class="text-center">
                    Ciao {{Auth::user()->name}}, qui puoi vedere gli annunci che hai rifiutato e deciderli se eliminarli definitivamente o ripristinarli.
                </h6>
            </div>
        </div>
    </div>

    @foreach ($ads as $ad)
    @if ($ad)
    <div class="container p-border shadow mt-4">
        <div class="row justify-content-center">
            <div class="col-12">
                <div class="card mx-auto p-border shadow">
                    <div class="p-3 bg-footer shadow">
                        <p class="text-center text-white m-0">{{ __('ui.annuncio')}} # {{$ad->id}}</p>
                    </div>
                </div>
            </div>
        </div>

        <div class="row justify-content-around align-items-center p-border-bottom mt-3">
            {{-- TITOLO UTENTE --}}
            <div class="col-3 col-md-3">
                <h3 class="text-start">{{ __('ui.utente')}}</h3>
            </div>
            {{-- NOME E EMAIL UTENTE --}}
            <div class="col-4 col-md-3">
                    <p class="text-start py-1">
                        <strong>Utente numero:</strong>
                    </p>
                    <p class="text-start py-1">
                        <strong>Nome:</strong>
                    </p>
                    <p class="text-start py-1">
                        <strong>E-mail:</strong>
                    </p>
            </div>
            <div class="col-4 col-md-3">
                <p class="text-start py-1">
                    {{$ad->user->id}}
                </p>
                <p class="text-start py-1">
                    {{$ad->user->name}}
                </p>
                <p class="text-start py-1">
                    {{$ad->user->email}}
                </p>
            </div>
        </div>

        <div class="row justify-content-around align-items-center p-border-bottom mt-3">
            <div class="col-3 col-md-3">
                <h3 class="text-start">{{ __('ui.titolo')}}</h3>
            </div>
            <div class="col-7 col-md-7">
                <p class="text-center m-0 py-1">{{$ad->title}}</div></p>
        </div>

        <div class="row justify-content-around align-items-center p-border-bottom mt-3">
            <div class="col-3 col-md-3">
                <h3 class="text-start">{{ __('ui.descrizione')}}</h3></div>
            <div class="col-7 col-md-7">
                <p class="text-center m-0 py-1">
                    {{$ad->description}}</div>
                </p>
        </div>

        <div class="row justify-content-around align-items-center p-border-bottom mt-3">
            <div class="col-12">
                <h3 class="text-center">{{ __('ui.immagine')}}</h3>
            </div>
            <div class="col-12 my-3">
                    @foreach ($ad->images as $image)

                    <div class="d-flex my-3">
                        <img
                            src="{{ $image->getUrl(400, 300) }}"
                            {{-- src="{{ Storage::url($image->file) }}" --}}
                            class="rounded img-fluid mx-auto" alt="">
                    </div>
            </div>
        </div>

        <div class="row justify-content-around align-items-center p-border-bottom mt-3">
            <div class="col-12 col-md-6 my-2">
                <x-progresbar
                    name="Adult"
                    value="{{$image->adult}}"/>
                <x-progresbar
                    name="Spoof"
                    value="{{$image->spoof}}"
                    />
                <x-progresbar
                    name="Medical"
                    value="{{$image->medical}}"
                    />
                <x-progresbar
                    name="Violence"
                    value="{{$image->violence}}"
                    />
                <x-progresbar
                    name="Racy"
                    value="{{$image->racy}}"
                    />
            </div>
            <div class="col-12 col-md-6 my-2">
                <ul>
                    @if ($image->labels)
                        @foreach ($image->labels as $label)
                            <li>{{ $label }}</li>
                        @endforeach
                    @endif
                </ul>
            </div>
        </div>
        @endforeach




    </div>

    @endif
    @endforeach

    <div class="container">
        <div class="row justify-content-around align-items-center mt-3">
            <div class="col-6 col-md-6">
                <form class="d-flex" method="POST" action="{{route('revisor.restore', ['id'=>$ad->id])}}">
                    @csrf
                    <button type="submit" class="btn btn-success mx-auto">{{ __('ui.ripristina')}}</button>
                </form>
            </div>
            <div class="col-6 col-md-6">
                <form class="d-flex" method="POST" action="{{route('revisor.delete', ['id'=>$ad->id])}}">
                    @csrf
                        @method('DELETE')
                    <button type="submit" class="btn btn-danger mx-auto">{{ __('ui.elimina definitivamente')}}</button>
                </form>
            </div>
        </div>
    </div>

</x-layout>
