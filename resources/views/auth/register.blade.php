<x-layout>
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-12 p-2 m-2">
                <h1 class="text-center primary m-3">{{ __('ui.registrati')}}</h1>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-12 col-md-8">
                <div class="card shadow-lg p-3 mb-5 bg-body rounded">
                    <div class="card-header-auth text-white p-3 mb-2 badge fw-bold fs-5">{{ __('ui.registrati')}}</div>
                    <div class="card-body ">
                        <form method="POST" action="{{ route('register') }}">
                            @csrf
                            <div class="form-group row m-2">
                                <label for="name" class="col-md-4 col-form-label text-md-right">{{ __('ui.nome')}}</label>
                                <div class="col-md-6">
                                    <input id="name" type="text" placeholder="Il tuo nome" class="form-control @error('name') is-invalid @enderror" name="name" value="{{ old('name') }}" required autocomplete="name" autofocus>
                                    @error('name')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>
                            <div class="form-group row m-2">
                                <label for="email" class="col-md-4 col-form-label text-md-right">{{ __('ui.e-mail')}}</label>
                                <div class="col-md-6">
                                    <input id="email" type="email" placeholder="La tua e-mail" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email">
                                    @error('email')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>
                            <div class="form-group row m-2">
                                <label for="password" class="col-md-4 col-form-label text-md-right">{{ __('Password') }}</label>
                                <div class="col-md-6">
                                    <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="new-password">
                                    @error('password')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>
                            <div class="form-group row m-2">
                                <label for="password-confirm" class="col-md-4 col-form-label text-md-right">{{ __('ui.conferma password')}}</label>
                                <div class="col-md-6">
                                    <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required autocomplete="new-password">
                                </div>
                            </div>
                            <div class="form-group row justify-content-center">
                                {{-- <div class="col-md-6 offset-md-4"> --}}
                                    <p class="text-center">
                                        <button type="submit" class="btn p-btn mt-4">
                                            {{ __('ui.registrati')}}
                                        </button>
                                    </p>
                                {{-- </div> --}}
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</x-layout>
