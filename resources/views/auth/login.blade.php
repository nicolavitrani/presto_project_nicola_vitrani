<x-layout>
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-12 p-2 m-2">
                <h1 class="text-center primary m-3">{{ __('ui.effettua il login')}}</h1>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-12 col-md-8">
                <div class="card shadow-lg p-3 mb-5 bg-body">
                    <div class="card-header-auth text-white p-3 mb-2 badge fw-bold fs-5">{{ __('ui.effettua il login')}}</div>
                    <div class="card-body p-0">
                        <form method="POST" action="{{ route('login') }}">
                            @csrf
                            <div class="form-group row m-3">
                                <label for="email" class="col-md-4 col-form-label text-md-right">{{ __('ui.e-mail') }}</label>
                                <div class="col-md-6">
                                    <input id="email" type="email" placeholder="Inserisci il tuo indirizzo e-mail" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>
                                    @error('email')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>
                            <div class="form-group row m-3">
                                <label for="password" class="col-md-4 col-form-label text-md-right">{{ __('ui.password') }}</label>
                                <div class="col-md-6">
                                    <input id="password" type="password" placeholder="Inserisci la tua password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="current-password">
                                    @error('password')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>
                                <div class="row m-3 mb-4">
                                    <div class="col-12 col-md-6">
                                        <div class="form-check">
                                            <input class="form-check-input" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>
                                            <label class="form-check-label" for="remember">
                                                {{ __('ui.ricordami')}}
                                            </label>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group row m-2 mb-0">
                                    {{-- <div class="col-12 col-md-8 mt-4"> --}}
                                        <p class="text-center">
                                            <button type="submit" class="btn p-btn">
                                                {{ __('Login') }}
                                            </button>
                                        </p>
                                    {{-- </div> --}}
                                    <div class="col 12 col-md-8"></div>
                                        @if (Route::has('password.request'))
                                            <a class="btn forgot-btn" href="{{ route('password.request') }}">
                                                {{ __('ui.password dimenticata')}}
                                            </a>
                                        @endif
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

</x-layout>
