<x-layout>

    @if ($ad)
    <div class="container mt-4">
        <div class="row">
            <div class="col-12">
                <h1 class="text-center">
                    {{ __('ui.area revisore') }}
                </h1>
            </div>
        </div>
    </div>
    <div class="container mt-4">
        <div class="row">
            <div class="col-12">
                <h6 class="text-center">
                    Ciao {{Auth::user()->name}}, qui puoi revisionare gli annunci degli utenti e decidere se verranno pubblicati o meno. <strong>Buon Lavoro!</strong>
                </h6>
            </div>
        </div>
    </div>

    <div class="container mt-5">
        <div class="row justify-content-center">
            <div class="col-12">
                <div class="card mx-auto p-border shadow">

                    <div class="d-flex mx-auto card-body p-2">
                        <h3 class="p-2 my-2">{{$ad->title}}</h3>
                    </div>

                    @foreach ($ad->images as $image)
                        <div class="container">
                            <div class="row p-4">
                                <div class="col-12 col-md-4">
                                    <img src="{{ $image->getUrl(400, 300) }}" class="rounded p-2 img-fluid mx-auto my-2" alt="Immagine annuncio">
                                </div>
                                <div class="col-12 col-md-4 my-3">
                                    <x-progresbar
                                    name="Adult"
                                    value="{{$image->adult}}"
                                    />
                                    <x-progresbar
                                    name="Spoof"
                                    value="{{$image->spoof}}"
                                    />
                                    <x-progresbar
                                    name="Medical"
                                    value="{{$image->medical}}"
                                    />
                                    <x-progresbar
                                    name="Violence"
                                    value="{{$image->violence}}"
                                    />
                                    <x-progresbar
                                    name="Racy"
                                    value="{{$image->racy}}"
                                    />
                                </div>
                                <div class="col-12 col-md-4">
                                    <div>
                                        <ul>
                                            @if ($image->labels)
                                                @foreach ($image->labels as $label)
                                                    <li>{{ $label }}</li>
                                                @endforeach
                                            @endif
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endforeach

                    <div class="row">
                        <div class="col-6 me-auto">
                            <p class="text-start ms-3 p-2">Categoria: <strong><a class="p-category" href="{{route('ad.category', [$ad->category->name, $ad->category->id])}}">{{$ad->category->name}}</a></strong></p>
                        </div>
                        <div class="col-6 ms-auto">
                            <p class="text-end me-3 p-2 mx-2">{{$ad->created_at->format('d/m/Y')}}</p>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-12">
                            <p class="text-center p-2 mx-2">Autore: {{$ad->user->name}}</p>
                        </div>
                    </div>

                    <div class="flex p-2">
                      <p class="card-text p-2 text-center">{{$ad->description}}</p>
                    </div>
                    <div class="row mx-auto p-2">
                        <div class="col-6 my-3 pb-2">
                            <form class="d-flex mx-auto" method="POST" action="{{route('revisor.reject', ['id' =>$ad->id])}}">
                                @csrf
                                <button type="submit" class="btn btn-danger mx-auto p-2">{{ __('ui.rifiuta')}}</button>
                            </form>
                        </div>
                        <div class="col-6 my-3 pb-2">
                            <form class="d-flex mx-auto" method="POST" action="{{route('revisor.accept', ['id' =>$ad->id])}}">
                                @csrf
                                <button type="submit" class="btn btn-success mx-auto p-2">{{ __('ui.accetta')}}</button>
                            </form>
                        </div>

                    </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    @else
    <div class="container my-5">
        <div class="row mx-auto">
            <div class="col-12">
                <h2 class="text-center">{{ __('ui.non ci sono articoli')}}</h2>
            </div>
        </div>
    </div>
    @endif
</x-layout>







