<x-layout>
    <x-masthead-h/>
    <h1 class="display-3"> Presto </h1>
    <x-masthead-f/>
<div class="container">
    <div class="row justify-content-center mt-5 mb-5">
        <div class="col-md-12 text-center">
            <h1>Annunci per categoria: {{ $category->name}}</h1>
        </div>
    </div>

    @foreach ($ads as $ad)
        <div class="row justify-content-center mb-5">
            <div class="col-md-8">
                <div class="cardcard-header text-dark shadow-lg p-3 mb-5 bg-body rounded">
                    <div class="card-header">{{$ad->title}}</div>
                    <div class="card-body badge">
                        <p>
                            <img src="https://via.placeholder.com/150/350"
                            class="rounded float-right" alt="fotosegnaposto">
                        {{$ad->description}}
                        </p>
                        <a href="{{route('ad.dettaglio',compact('ad'))}}" class="btn p-btn m-3">Vai al dettaglio</a>
                    </div>

                    <div class="card-footer d-flex justify-content-between">
                        <strong><p>Categoria: {{ $ad->category->name }}</p></strong>
                        {{$ad->created_at->format('d/m/Y')}} - {{$ad->user->name}}</i>

                    </div>
                </div>
            </div>
        </div>
    @endforeach

    <div class="row justify-content-center">
        <div class="col-md-8">
            {{ $ads->links() }}
        </div>
    </div>
</div>
</x-layout>
