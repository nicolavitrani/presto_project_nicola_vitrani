<footer class="mt-5 pt-3 p-bg-footer">
    <div class="container-fluid pb-5">
        <div class="row">
            <div class="col-12 col-md-4 mt-4">
                <p class="fw-bold text-center text-white">Contatti:</p>
                <p class="text-center"><a class="p-link-footer" href="tel:3924163838"><i class="fab fa-whatsapp"></i> 3924163838</a></p>
                <p class="text-center"><a class="p-link-footer" href="mailto:nicolavitrani93@gmail.com"><i class="far fa-envelope"></i> nicolavitrani93@gmail.com</a></p>
            </div>
            <div class="col-12 col-md-4 mt-4">
                <p class="fw-bold text-center text-white">I miei social:</p>
                <p class="text-center"><a class="p-link-footer" href="https://www.facebook.com/nicola.vitrani/"><i class="fab fa-facebook-square"></i> Facebook</a></p>
                <p class="text-center"><a class="p-link-footer" href="https://www.instagram.com/nicolavitrani/"><i class="fab fa-instagram"></i> Instagram</a></p>
            </div>
            <div class="col-12 col-md-4 mt-4">
                <p class="fw-bold text-center text-white">La mia repository Gitlab:</p>
                <p class="text-center"><a class="p-link-footer" href="https://gitlab.com/nicolavitrani"><i class="fab fa-gitlab"></i> Gitlab</a></p>
            </div>
        </div>

        <div class="row justify-content-center">
            <div class="col-10 col-md-3 mt-3 mb-1">
                <img class="img-fluid" src="/img/logo_white.png" alt="Logo Presto.it">
            </div>
        </div>

        <div class="row">
            <div class="col-12">
                <p class="text-white text-center">Developed by <strong>Nicola Vitrani</strong></p>
            </div>
        </div>
    </div>
</footer>
