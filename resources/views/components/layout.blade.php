<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="csrf-token" content="{{csrf_token()}}">

    {{-- link_icon_bootstrap --}}
    <link href="https://cdn.deliver.net/npm/bootstrap-icons@1.4.1/font/bootstrap-icons.css" rel="stylesheet" />
    <!-- Link_FontAwesome -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/css/all.min.css">
    {{-- My Favicon --}}
    <link rel="icon" type="image/png" sizes="192x192" href="/Favicon/icon.png">
    <link rel="stylesheet" href="{{asset('css/app.css')}}">
    <title>Presto.it</title>


</head>
<body>
    <div class="nav-space">
        <p class="transparent">Nav space</p>
    </div>

    <x-nav2></x-nav2>

        {{$slot}}

    <x-foot2></x-foot2>

    <script src="{{asset('js/app.js')}}"></script>
    {{-- <script type="text/javascript" src="vanilla-tilt.js"></script> --}}
    {{-- <script type="text/javascript">
        VanillaTilt.init(document.querySelectorAll(".cardmove"), {
            max: 25,
            speed: 400
        });
    </script> --}}

</body>
</html>
