
<!-- subscribe -->
<div class="container margt">
    <div class="row">
        <div class="col-md-10 col-lg-8 mx-auto text-center">
            <i class="far fa-paper-plane fa-3x mb-2 bg-castom"></i>
            <h2 class="text-dark mb-5">Subscribe to receive updates!</h2>
            <form class="form-inline d-flex ">
                <input class="form-control flex-fill mr-0 mr-sm-2  mb-sm-0 form-control btn-outline2-danger text-dark" id="inputEmail" type="email" placeholder="Enter email address..." />
                <button class="btn btn-outline-danger fw-bold mx-3 " type="submit">Subscribe</button>
            </form>
        </div>
    </div>
</div>
