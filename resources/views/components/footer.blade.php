
<!-- Footer -->
{{-- <div class="container bg-black margt">
    <div class="row">

        <div class="col-md-4 mb-3 mb-md-0">
            <div class="card py-4 h-100">
                <div class="card-body text-center">
                    <i class="fas fa-map-marked-alt fa-3x mb-2 bg-castom"></i>
                    <h4 class="text-uppercase m-0 text-black">Address</h4>
                    <hr class="my-4" />
                    <div class="small active"> 00165 Cardinal Pacca N.13 , Roma IT</div>
                </div>
            </div>
        </div>


        <div class="col-md-4 mb-3 mb-md-0">
            <div class="card py-4 h-100">
                <div class="card-body text-center">
                    <i class="fas fa-envelope fa-3x mb-2 bg-castom"></i>
                    <h4 class="text-uppercase m-0 text-black">Email</h4>
                    <hr class="my-4" />
                    <div class="small "><a class= "color-tx-area" href="#!">Laravel@domin.erf.com</a></div>
                </div>
            </div>
        </div>


        <div class="col-md-4 mb-3 mb-md-0">
            <div class="card py-4 h-100">
                <div class="card-body text-center">
                    <i class="fas fa-mobile-alt fa-3x mb-2 bg-castom"></i>
                    <h4 class="text-uppercase m-0 text-black">Phone</h4>
                    <hr class="my-4" />
                    <div class="small active">+39 (555) 902-8832</div>
                </div>
            </div>
        </div>

    </div>


    <div class="social d-flex justify-content-center mt-5 mb-5">
        <a class="mx-2" href="https://twitter.com/"><i class="fab fa-twitter bg-castom fa-2x me-2"></i></a>
        <a class="mx-2" href="https://www.facebook.com/"><i class="fab fa-facebook-f bg-castom fa-2x me-2"></i></a>
        <a class="mx-2" href="https://github.com/"><i class="fab fa-github bg-castom fa-2x me-2"></i></a>
    </div>

</div>

    <!-- Copyrights -->
    <div class="bg-dark py-4">
        <div class="container text-center">
        <p class="text-muted mb-0 py-2 active">© 2021 Christian-Pinto/ Nicola-Bellomo/ Nicola-Vitrani/ Valeria-Riccardi All rights reserved.</p>
        </div>
    </div>
</footer> --}}


<footer class="new_footer_area bg_color">
    <div class="new_footer_top">
        <div class="container-fluid p-4">
            <div class="row">
                <div class="col-12 col-md-5 col-lg-3">
                    <div class="f_widget company_widget wow fadeInLeft" data-wow-delay="0.2s" style="visibility: visible; animation-delay: 0.2s; animation-name: fadeInLeft;">
                        <h3 class="f-title f_600 t_color f_size_18">{{ __('ui.contattaci')}}</h3>
                        <p>{{ __('ui.aggiornamento')}}!</p>
                        <form action="#" class="f_subscribe_two mailchimp" method="" novalidate="true" _lpchecked="1">
                            <input type="text" name="EMAIL" class="form-control memail" placeholder="Email">
                            <p class="mchimp-errmessage" style="display: none;"></p>
                            <p class="mchimp-sucmessage" style="display: none;"></p>
                        </form>
                        <div class="row justify-content-between mt-3">
                            <div class="col-md-6">
                                <button class="btn btn_get btn_get_two" type="submit">{{ __('ui.sottoscrivi')}}</button>
                            </div>
                            <div class="col-md-6 text-end">
                                <a class="btn btn_get btn_get_two" href="{{route('contacts')}}">{{ __('ui.mail')}}</a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6">
                    <div class="f_widget about-widget pl_70 wow fadeInLeft" data-wow-delay="0.4s" style="visibility: visible; animation-delay: 0.4s; animation-name: fadeInLeft;">
                        <h3 class="f-title f_600 t_color f_size_18">{{ __('ui.scarica')}}</h3>
                        <ul class="list-unstyled f_list">
                            <li><a href="#">{{ __('ui.azienda')}}</a></li>
                            <li><a href="#">{{ __('ui.android')}}</a></li>
                            <li><a href="#">{{ __('ui.ios')}}</a></li>
                            <li><a href="#">{{ __('ui.desktop')}}</a></li>
                            <li><a href="#">{{ __('ui.progetti')}}</a></li>
                            <li><a href="#">{{ __('ui.compiti')}}</a></li>
                        </ul>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6">
                    <div class="f_widget about-widget pl_70 wow fadeInLeft" data-wow-delay="0.6s" style="visibility: visible; animation-delay: 0.6s; animation-name: fadeInLeft;">
                        <h3 class="f-title f_600 t_color f_size_18">{{ __('ui.aiuto')}}</h3>
                        <ul class="list-unstyled f_list">
                            <li><a href="#">{{ __('ui.faq')}}</a></li>
                            <li><a href="#">{{ __('ui.termini')}} &amp; {{ __('ui.condizioni')}}</a></li>
                            <li><a href="#">{{ __('ui.segnalazioni')}}</a></li>
                            <li><a href="#">{{ __('ui.documentazione')}}</a></li>
                            <li><a href="#">{{ __('ui.supporto')}}</a></li>
                            <li><a href="#">{{ __('ui.privacy')}}</a></li>
                        </ul>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6">
                    <div class="f_widget social-widget pl_70 wow fadeInLeft" data-wow-delay="0.8s" style="visibility: visible; animation-delay: 0.8s; animation-name: fadeInLeft;">
                        <h3 class="f-title f_600 t_color f_size_18">{{ __('ui.soluzioni')}}</h3>
                        <div class="f_social_icon">
                            <a href="#" class="fab fa-facebook"></a>
                            <a href="#" class="fab fa-twitter"></a>
                            <a href="#" class="fab fa-linkedin"></a>
                            <a href="#" class="fab fa-pinterest"></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="footer_bg">
            <div class="footer_bg_one"></div>
            <div class="footer_bg_two"></div>
        </div>
    </div>
    <div class="footer_top">
        <div class="container-fluid">
            <div class="bg-dark py-4">
                <div class="container text-center">
                <p class="text-muted mb-0 py-2 active">© 2021 Christian-Pinto/ Nicola-Bellomo/ Nicola-Vitrani/ Valeria-Riccardi {{ __('ui.diritti')}}.</p>
                </div>
            </div>
        </div>
    </div>
</footer>

