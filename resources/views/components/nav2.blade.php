<nav class="navbar navbar-expand-lg navbar-dark nav-border fixed-top p-bg-nav">
    <div class="container-fluid">
      <a class="navbar-brand" href="{{route('home')}}"><img style="height: 60px" src="/img/logo_white.png" alt="logo"></a>
      <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>
      <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="navbar-nav ms-auto mb-2 mb-lg-0">
          <li class="nav-item">
            <a class="nav-link navbar-link text-white text-center text-uppercase mx-2" aria-current="page" href="{{route('home')}}"><i class="fas fa-home"></i></a>
          </li>
          <li class="nav-item">
            <a class="nav-link navbar-link text-white text-center text-uppercase mx-2" href="{{route('ad.create')}}"><i class="fas fa-plus-square"></i></a>
          </li>
           <li class="nav-item dropdown">
            <a class="nav-link dropdown-toggle navbar-link text-white text-center text-uppercase" href="#" id="navbarDropdown" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                <i class="fas fa-list"></i>
            </a>
                <ul class="dropdown-menu" aria-labelledby="navbarDropdown">
                    @foreach ($categories as $category)

                        <a class="my-cl nav-link text-center text-dark text-uppercase"
                        href="{{route('ad.category', [
                            $category->name,
                            $category->id
                            ])}}"> {{ $category->name}} </a>
                        <li><hr class="dropdown-divider"></li>
                    @endforeach
                    </ul>
                </li>



                @guest
                <li class="nav-item">
                    <a class="nav-link navbar-link text-white text-center text-uppercase mx-2" href="{{route('login')}}">Login <i class="fas fa-sign-in-alt"></i></a>
                </li>
                <li class="nav-item">
                    <a class="nav-link navbar-link text-white text-center text-uppercase mx-2" href="{{route('register')}}">{{ __('ui.registrati')}} <i class="fas fa-file-signature"></i></a>
                </li>
                @else
                @if(Auth::user() && Auth::user()->is_revisor)
                     <li class="nav-item">
                         <a class="nav-link navbar-link text-white text-center text-uppercase mx-2" href="{{route('auth.revisor')}}">
                            {{ __('ui.area revisore')}}
                             <span class="badge badge-pill text-light">
                                 {{\App\Models\Ad::ToBeRevisionedCount()}}
                             </span>
                         </a>
                     </li>

                     <li class="nav-item">
                        <a class="nav-link navbar-link text-white text-center text-uppercase mx-2" href="{{route('revisor.trash')}}">
                            {{ __('ui.cestino')}} <i class="fas fa-trash-alt"></i>
                            <span class="badge badge-pill text-light">
                                {{\App\Models\Ad::TrashedCount()}}
                            </span>
                        </a>
                    </li>
                 @endif


                <div class="btn-group">
                    <button type="button" class="btn px-2 fs-6 nav-link navbar-link text-white text-center text-uppercase" data-bs-toggle="dropdown" aria-expanded="false">
                        {{ __('ui.ciao')}}, {{Auth::user()->name}} <i class="fas fa-caret-down"></i></i>
                    </button>
                    <ul class="dropdown-menu" aria-labelledby="navbarDropdown">
                        <li><a class="dropdown-item" href="{{route('logout')}}" onclick="event.preventDefault();document.getElementById('logout').submit();">Logout</a></li>
                            <form method="POST"action="{{route('logout')}}" id="logout">
                                @csrf
                            </form>
                        </li>
                    </ul>
                </div>
            @endguest
            <li class="nav-item">
                <a class="nav-link navbar-link text-white text-center text-uppercase mx-2" href="{{route('ad.index')}}"><i class="fas fa-bullhorn"></i></a>
            </li>
            <li class="nav-item navbar-link text-white text-center text-uppercase">
                <form method="POST" action="{{ route('locale', 'it')}}">
                @csrf
                    <button type="submit" class="nav-link mx-auto" style='background-color:transparent; border:none;'>
                        <span class="flag-icon flag-icon-it text-center"></span>
                    </button>
                </form>
            </li>
            <li class="nav-item navbar-link text-white text-center text-uppercase">
                <form method="POST" action="{{ route('locale', 'en')}}">
                    @csrf
                    <button type="submit" class="nav-link mx-auto" style='background-color:transparent; border:none;'>
                        <span class="flag-icon flag-icon-gb"></span>
                    </button>
                </form>
            </li>
            <li class="nav-item navbar-link text-white text-center text-uppercase">
                <form method="POST" action="{{ route('locale', 'es')}}">
                    @csrf
                    <button type="submit" class="nav-link mx-auto" style='background-color:transparent; border:none;'>
                        <span class="flag-icon flag-icon-es"></span>
                    </button>
                </form>
            </li>
        </ul>
      </div>
    </div>
</nav>


