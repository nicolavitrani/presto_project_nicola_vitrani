<x-layout>

    <div class="container">
        <div class="row">
            <div class="col-12 col-md-6 offset-md-3 text-center mt-3">
                <h1>{{ __('ui.modifica il tuo annuncio') }}</h1>
            </div>
        </div>
    </div>

    <div class="container">
        <div class="row justify-content-center">
            @if(session('message'))
                <div class="alert alert-success">
                    {{session('message')}}
                </div>
            @endif
        </div>
    </div>

    <div class="container">
        <div class="row mt-5">
            @if ($errors->any())
                <div class="col-10 alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
        </div>
    </div>


    <div class="container">
        <div class="row justify-content-center">
            <div class="col-12 col-md-10">
                <form method="POST" action="{{route('ad.update', compact('ad'))}}" enctype="multipart/form-data">
                    @csrf
                    <div class="mb-3">
                        <label for="exampleInputEmail1" class="form-label">Titolo</label>
                        <input type="text" class="form-control" name="title" value="{{$ad->title}}">
                    </div>
                    <div class="mb-3 text-center">
                        <textarea name="description" style="width: 100%" rows="7" placeholder="Inserisci una descrizione">{{$ad->description}}</textarea>
                    </div>
                    <div class="text-center">
                        <button type="submit" class="btn p-btn">{{ __('ui.modifica il tuo annuncio') }}</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</x-layout>
