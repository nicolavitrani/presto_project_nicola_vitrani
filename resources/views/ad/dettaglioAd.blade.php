<x-layout>

    <div class="container mt-4">
        <div class="row">
            <div class="col-12">
                <h1 class="text-center">
                    {{ __('ui.dettaglio annuncio') }}
                </h1>
            </div>
        </div>
    </div>

    <div class="container mt-5">
        <div class="row justify-content-center">
            <div class="col-12 col-md-10">
                <div class="card mx-auto p-border shadow">

                    <div class="d-flex mx-auto card-body p-2">
                        <h5 class="p-2">{{$ad->title}}</h5>
                    </div>

                    @foreach ($ad->images as $image)
                        <div class="p-2 d-flex">
                            <img src="{{ $image->getUrl(400, 300) }}" class="rounded p-2 img-fluid mx-auto" alt="Immagine annuncio" style="width: 70%">
                        </div>
                    @endforeach

                    <div class="d-flex">
                        <div class="col-6 me-auto">
                            <p class="text-start ms-3 p-2">Categoria: <strong><a class="p-category" href="{{route('ad.category', [$ad->category->name, $ad->category->id])}}">{{$ad->category->name}}</a></strong></p>
                        </div>
                        <div class="col-6 ms-auto">
                            <p class="text-end me-3 p-2 mx-2">{{$ad->created_at->format('d/m/Y')}}</p>
                        </div>
                    </div>
                    <div class="d-flex">
                        <div class="col-12">
                            <p class="text-center p-2 mx-2">Autore: {{$ad->user->name}}</p>
                        </div>
                    </div>

                    <div class="flex p-2">
                      <p class="card-text p-2 text-center">{{$ad->description}}</p>
                    </div>
                    <div class="row mx-auto p-2">
                        <div class="col-6 my-3 pb-2">
                            <form class="mx-auto" method="POST" action="{{route('ad.delete', compact('ad'))}}">
                                @method('DELETE')
                                @csrf
                                <button type="submit" class="btn btn-danger mx-auto p-2">{{ __('ui.elimina')}}</button>
                            </form>
                        </div>
                        <div class="col-6 my-3 pb-2">
                            <a href="{{route('ad.edit', compact('ad'))}}" class="btn btn-warning mx-auto p-2">{{ __('ui.modifica')}}</a>
                        </div>

                    </div>
                    </div>
                </div>
            </div-col-12>
        </div>
    </div>

    <div class="container mt-5">
        <div class="row">
            <div class="col-12 p-2">
                <p class="text-center">
                    <a href="{{route('ad.index')}}" class="mx-auto btn p-btn">{{ __('ui.tutti gli annunci') }}</a>
                </p>
            </div>
        </div>
    </div>


</x-layout>

