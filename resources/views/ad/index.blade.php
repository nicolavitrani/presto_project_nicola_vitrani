<x-layout>
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-12 col-md-3 mt-4 mb-4">
                <h1 class="text-center primary">
                    {{ __('ui.tutti gli annunci')}}
                </h1>
            </div>
        </div>
    </div>
        <div class="row justify-content-around m-3">
            @foreach ($ads as $ad)
                <div class="col-12 col-md-5 text-center m-3">
                    <div class="row justify-content-center">
                        <div data-tilt data-tilt-axis="x">
                        <div class="card mb-5 card-border shadow-lg p-3 mb-5 bg-body rounded ">
                            <div class="card-body">
                                <h5 class="card-title primary m-2">{{$ad->title}}</h5>
                                <p class="card-text m-2">{{$ad->description}}</p>
                                <p >{{$ad->price}}</p>
                                <a href="{{route('ad.dettaglio',compact('ad'))}}" class="btn p-btn m-3">{{ __('ui.vai al dettaglio')}}</a>
                            </div>
                        </div>
                    </div>
                    </div>
                </div>
            @endforeach
        </div>

        {{-- <div class="row justify-content-center">
            <div class="col-md-8">
                {{ $ads->links() }}
            </div>
        </div> --}}
</x-layout>
