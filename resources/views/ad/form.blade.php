<x-layout>
    <div class="container p-0 p-md-5">
    @if (session('message'))
    <div class="row justify-content-center">
        <div class="col-lg-6 col-md-6 col-sm-3 mt-5">
            <div class="alert alert-success text-center fw-bold">
                <p>
                    L'annuncio e stato inserito correttamente! 😊
                </p>
                <p>
                    Attualmente è in stato di valutazione, verrà publicato a breve.
                </p>
            </div>
        </div>
    </div>
    @endif


    <div class="container ">
        <div class="row">
            <div class="col-12 my-4">
                <h1 class="primary text-center">
                    {{ __('ui.crea il tuo annuncio')}}
                </h1>
            </div>
        </div>

        @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif

        {{-- <div class="fw-bold">
            <h3>DEBUG:: SECRET {{ $uniqueSecret }}
        </div> --}}

        <form method="POST" action="{{route('ad.store')}}" enctype="multipart/form-data">
            @csrf

            <input type="hidden" name="uniqueSecret" value="{{ $uniqueSecret }}">

            <div class="row m-4 justify-content-end">
                <div for="category" class="col-4 col-md-2 text-end secondary">
                    <p class="text-center p-2"><strong>{{ __('ui.categoria')}}</strong></p>
                </div>
                <div class="col-8 col-md-2">
                    <select name="category_id" id="category" class="p-select p-2">
                        @foreach ($categories as $category)
                            <option value="{{ $category->id }}" {{old('category') == $category->id ? 'selected' :  ''}}>{{ $category->name }}</option>
                        @endforeach
                    </select>
                </div>
            </div>
            <div class="row">
                <div class="mb-3">
                    <label for="exampleFormControlInput1" class="form-label"><strong>{{ __('ui.titolo annuncio')}}</strong></label>
                    <input type="text" class="form-control" id="exampleFormControlInput1" placeholder="{{ __('ui.inserisci il titolo')}}" name="title" value='{{old('title')}}'>
                </div>
                <div class="mb-3">
                    <label for="exampleFormControlTextarea1" class="form-label"><strong>{{ __('ui.descrizione')}}</strong></label>
                    <textarea class="form-control" id="exampleFormControlTextarea1" placeholder="{{ __('ui.inserisci la descrizione')}}" rows="3" name="description">{{old('description')}}</textarea>
                    <label for="exampleFormControlTextarea1" class="form-label m-2"><strong>{{ __('ui.prezzo')}}</strong></label>
                    <input type="integer" class="form-control" id="exampleFormControlInput1" placeholder="{{ __('ui.inserisci il prezzo')}}" name="price" value='{{old('price')}}'>

                    <div class="col-12">
                        <div class="mx-auto mt-4 mb-3">
                            <label for="images" class="col-11"><strong>{{ __('ui.immagini')}}</strong></label>
                        </div>
                        <div class="d-flex justify-content-center mt-2">
                                <div class="col-11">
                                    <div class="dropzone" id="drophere">
                                        @error('images')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                    </div>
                                </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class=" col-12 mt-4">
                            <p class="text-center">
                                <button type="submit" class="btn p-btn ps-5 pe-5 fw-bold">{{ __('ui.invia')}}</button>
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>
</x-layout>
