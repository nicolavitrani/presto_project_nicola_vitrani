<x-layout>
  <h3 class="mt-3"> Risultati Ricerca per: {{ $q }}</h3>
  <div class="row">
      @foreach ($ads as $ad)
        <div class="col-lg-6">
            <div class="row justify-content-center mb-5">
                <div class="col-md-8">
                    <div class="cardcard-header text-dark shadow-lg p-3 mb-5 bg-body rounded ">
                        <div class="card-header">{{$ad->title}}</div>
                        <div class="card-body badge">
                            <p>
                                <img src="https://via.placeholder.com/150/350"
                                class="rounded float-right" alt="fotosegnaposto">
                            {{$ad->description}}
                            </p>
                            <a href="{{route('ad.dettaglio',compact('ad'))}}" class="btn p-btn m-3">Vai al dettaglio</a>
                        </div>

                        {{-- <div class="card-footer d-flex justify-content-between">
                            <strong><p>Categoria: {{ $ad->category->name }}</p></strong>
                            {{$ad->created_at->format('d/m/Y')}} - {{$ad->user->name}}</i>

                        </div> --}}
                    </div>
                </div>
            </div>
        </div>
      @endforeach
  </div>
</x-layout>
