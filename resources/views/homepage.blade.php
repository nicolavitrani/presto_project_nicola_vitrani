<x-layout>
    @if (session('access.denied.revisor.only'))
    <div class="alert alert-danger fs-4 fw-bold m-0 text-center">
       {{ __('ui.message')}}
    </div>
    @endif
    <x-masthead-h/>
        <h1 class="display-3">{{ __('ui.welcome')}}</h1>
        <div class="row justify-content-center">
        <div class="col-sm-6 col-lg-6 col-sm-5 text-center">
        <form method="GET" action="{{route('search')}}" class="mt-3">
            <input class="form-control text-center mt-5" type="text" name="q" placeholder="{{ __('ui.ricerca')}}" style=""/>
            <button class="btn p-btn mt-5" type="submit">{{ __('ui.ricerca')}}</button>
        </form>
        </div>
        </div>
    <x-masthead-f/>


<!-- About Start -->
<section class="py-5 border-bottom">
    <div class="about wow fadeInUp" data-wow-delay="0.1s">
        <div class="container">
            <div class="row align-items-center">
                <div class="col-12 col-md-5">
                    <div class="about-img">
                        <img src="/img/Online Shopping.svg" alt="Image" class="img-fluid ">
                    </div>
                </div>
                <div class="col-12 col-md-5">
                    <div class="section-header text-left">
                        <h3 class="col1p primary">{{ __('ui.scopri')}}</h3>
                        <h2 class="col1p fw-bold">{{ __('ui.welcome')}}</h2>
                    </div>
                    <div class="about-text">
                        <p class="">
                            Siamo la digital company n.1 in in Italia per comprare e vendere. Siamo nati a Milano nel 2021 e ci posizioniamo stabilmente tra i primi 10 brand online più visitati in Italia con 11 milioni di utenti ogni mese.
                        </p>
                        <p class="">
                            Il nostro obiettivo è offrire il servizio online di compravendita più semplice, veloce e sicuro d’Italia. Per questo, persone e utenti professionali scelgono ogni giorno Subito per fare ottimi affari.
                        </p>
                        <div class="container">
                            <div class="row">
                                <div class="col-12">
                                    <p class="text-center mt-3 mb-2"><a class="btn p-btn" href="{{route('ad.index')}}">Vai agli annunci</a></p>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<!-- Features section-->
{{-- <section class="py-5 border-bottom" id="features">
    <div class="container px-5 my-5">
        <div class="row gx-5">
            <div class="col-lg-4 mb-5 mb-lg-0">
                <div class="feature bg-primary bg-gradient text-white rounded-3 mb-3"><i class="bi bi-collection"></i></div>
                <h2 class="h4 fw-bolder">Featured title</h2>
                <p>Paragraph of text beneath the heading to explain the heading. We'll add onto it with another sentence and probably just keep going until we run out of words.</p>
            </div>
            <div class="col-lg-4 mb-5 mb-lg-0">
                <div class="feature bg-primary bg-gradient text-white rounded-3 mb-3"><i class="bi bi-building"></i></div>
                <h2 class="h4 fw-bolder">Featured title</h2>
                <p>Paragraph of text beneath the heading to explain the heading. We'll add onto it with another sentence and probably just keep going until we run out of words.</p>
            </div>
            <div class="col-lg-4">
                <div class="feature bg-primary bg-gradient text-white rounded-3 mb-3"><i class="bi bi-toggles2"></i></div>
                <h2 class="h4 fw-bolder">Featured title</h2>
                <p>Paragraph of text beneath the heading to explain the heading. We'll add onto it with another sentence and probably just keep going until we run out of words.</p>

            </div>
        </div>
    </div>
</section> --}}

<!-- Service End -->
{{-- <section>
    <div id="myCarousel" class="carousel slide" data-bs-ride="carousel">
        <div class="carousel-indicators">
        <button type="button" data-bs-target="#myCarousel" data-bs-slide-to="0" class="active" aria-current="true" aria-label="Slide 1"></button>
        <button type="button" data-bs-target="#myCarousel" data-bs-slide-to="1" aria-label="Slide 2"></button>
        <button type="button" data-bs-target="#myCarousel" data-bs-slide-to="2" aria-label="Slide 3"></button>
        </div>
        <div class="carousel-inner" role="listbox" style=" width:100%; height: 512px">
        <div class="carousel-item active" style="background-image: url(/img/sfondo.jpg)">

            <div class="container">
                <div class="carousel-caption text-start">

                    <h1>{{ __('ui.pubblicità')}}</h1>
                    <p>{{ __('ui.sconto')}}</p>
                    <p><a class="btn btn-lg btn-primary">PITEURO2021</a></p>

                </div>
            </div>
        </div>
        <div class="carousel-item" style="background-image: url(/img/sfondo.jpg)">

            <div class="container">
                <div class="carousel-caption text-center mb-5">

                    <h1>{{ __('ui.secure')}}</h1>
                    <p>{{ __('ui.secure1')}}</p>

                </div>
            </div>
        </div>
        <div class="carousel-item" style="background-image: url(/img/sfondo.jpg)">

            <div class="container">
                <div class="carousel-caption text-end mb-5">

                    <h1>{{ __('ui.spedizioni')}}</h1>
                    <p>{{ __('ui.spedizioni1')}}</p>

                </div>
            </div>
        </div>
        </div>
        <button class="carousel-control-prev" type="button" data-bs-target="#myCarousel" data-bs-slide="prev">
        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
        <span class="visually-hidden">Previous</span>
        </button>
        <button class="carousel-control-next" type="button" data-bs-target="#myCarousel" data-bs-slide="next">
        <span class="carousel-control-next-icon" aria-hidden="true"></span>
        <span class="visually-hidden">Next</span>
        </button>
    </div>
</section> --}}

<!-- Testimonials section-->
{{-- <section class="py-5 border-bottom">
    <div class="container px-5 my-5 px-5">
        <div class="text-center mb-5">
            <h2 class="fw-bolder">{{ __('ui.customer')}}</h2>
            <p class="lead mb-0">{{ __('ui.customer1')}}</p>
        </div>
        <div class="row gx-5 justify-content-center">
            <div class="col-lg-6"> --}}

                <!-- Testimonial 1-->
            {{-- <div data-tilt data-tilt-scale="1.1">
                <div class="card mb-4 cardmove">
                    <div class="card-body p-4">
                        <div class="d-flex">
                            <div class="flex-shrink-0"><i class="bi bi-chat-right-quote-fill text-primary fs-1"></i></div>
                            <div class="ms-4">
                                <p class="mb-1">Facendo lo streamer di professione, ho bisognio costantemente di comprare e di ricevere in breve tempo i miei acquisti!<br> In questo Presto non mi ha mai deluso, sono sempre stati precisi con le cosegnie e i pacchi sono sempre arrivati in perfette condisioni.</p>
                                <div class="small text-muted">- Dario Moccia, Pisa</div>
                            </div>
                        </div>
                    </div>
                </div>
            </div> --}}


                <!-- Testimonial 2-->
            {{-- <div data-tilt data-tilt-scale="1.1">
                <div class="card">
                    <div class="card-body p-4 cardmove">
                        <div class="d-flex">
                            <div class="flex-shrink-0"><i class="bi bi-chat-right-quote-fill text-primary fs-1"></i></div>
                            <div class="ms-4">
                                <p class="mb-1">Sono un appasionato di manga e fumetti, e ci tengo moltissimo alle condizioni dei manga che aquisto.<br>Con presto vado compro sicuro, acquisti facili e sicuri, consegie rapide e pacchi sempre in perfette condizioni, non potrei chiedere di meglio.</p>
                                <div class="small text-muted">- Neel Frye, Torino</div>
                            </div>
                        </div>
                    </div>
                </div>
            </div> --}}
            {{-- </div>
        </div>
    </div>
</section> --}}
    <div class="container mt-5 mb-4">
        <div class="row">
            <div class="col-12">
                <h1 class="text-center">{{__('ui.gli ultimi annunci')}}</h1>
            </div>
        </div>
    </div>

    {{-- card dinamica --}}
    <div class="conteiner mt-5">
        <div class="row justify-content-around p-0 m-0">
            @foreach ($ads as $ad)
            <div class="col-12 col-md-7 mt-3">
                <div>
                    <div class="card shadow p-3 p-rounded mb-2">
                        <div class="card-header"><h4 class="text-white text-center pt-3">{{$ad->title}}</h4></div>
                        <div class="card-body">
                            <div class="row m-2 justify-content-around">
                                @foreach ($ad->images as $image)

                                            <div class="class col-12 col-md-6">
                                                <img
                                                    src="{{ $image->getUrl(300, 150) }}"
                                                    {{-- src="{{ Storage::url($image->file) }}" --}}
                                                    class="img-fluid py-2" alt="Immagine annuncio">
                                            </div>
                                            {{-- <div class="col-md-8"> --}}
                                                {{-- {{ $image->id }} --}}
                                                {{-- {{ $image->file }} --}}
                                                {{-- {{ Storage::url($image->file) }} --}}
                                            {{-- </div> --}}

                                @endforeach
                            </div>
                            <div class="col-12">
                                <p>

                                    {{$ad->description}}
                            </p>
                            </div>
                            <p class="text-center p-2">
                                <a href="{{route('ad.dettaglio', compact('ad'))}}" class="btn p-btn m-3">{{ __('ui.vai al dettaglio') }}</a>
                            </p>

                            {{-- <a href="{{route('ad.edit', compact('ad'))}}" class="btn p-btn mb-2">Modifica</a> --}}
                        </div>
                        <div class="card-footer d-flex justify-content-between shadow">
                            <div>
                                <strong>Categoria: <a class="secondary" style="text-decoration: none" href="{{route('ad.category', [
                                    $ad->category->name,
                                    $ad->category->id,
                                ])}}">{{ $ad->category->name }}</a></strong>
                            </div>

                            <div>
                                <strong><p class="text-end m-1"></strong>{{$ad->user->name}}, {{$ad->created_at->format('d/m/Y')}}</p>

                            </div>


                        </div>


                    </div>
                </div>
            </div>
            @endforeach
        </div>
    </div>

</x-layout>

