<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\AdController;
use App\Http\Controllers\PublicController;
use App\Http\Controllers\RevisorController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [PublicController::class, 'homepage'])->name('home');
Route::get('/category/{name}/{id}/ads', [PublicController::class, 'adsByCategory'])->name('ad.category');
Route::get('/creazione/annuncio', [AdController::class, 'create'])->name('ad.create');
//Rotte immagini
Route::get('/ad/images', [AdController::class, 'getImages'])->name('ad.images');
Route::post('/ad/images/upload', [AdController::class, 'uploadImage'])->name('ad.images.upload');
Route::delete('/ad/images/remove', [AdController::class, 'removeImage'])->name('ad.images.remove');
//
Route::post('/storead', [AdController::class, 'store'])->name('ad.store');
Route::get('/annunci', [AdController::class, 'index'])->name('ad.index');
Route::get('/annunci/dettaglio/{ad}', [AdController::class, 'show'])->name('ad.dettaglio');
Route::get('/annunci/modifica/{ad}', [AdController::class,'edit'])->name('ad.edit');
Route::post('/annuncio/update/{ad}', [AdController::class,'update'])->name('ad.update');
Route::delete('/annuncio/delete/{ad}', [AdController::class,'destroy'])->name('ad.delete');


//Revisor area//
Route::get('/revisor/home', [RevisorController::class, 'index'])->name('auth.revisor');
Route::post('/revisor/ad/{id}/accept' , [Revisorcontroller::class, 'accept'])->name('revisor.accept');
Route::post('/revisor/ad/{id}/reject' , [Revisorcontroller::class, 'reject'])->name('revisor.reject');

Route::get('/revisor/trash', [RevisorController::class, 'trash'])->name('revisor.trash');
Route::post('/revisor/ad/{id}/restore', [RevisorController::class, 'restore'])->name('revisor.restore');
Route::delete('/revisor/ad/{id}/delete', [RevisorController::class, 'delete'])->name('revisor.delete');

//Search Area//
Route::get('/search', [AdController::class, 'search'])->name('search');


//Email area//
Route::get('/contacts', [PublicController::class, 'contacts'])->name('contacts');
Route::post('/contacts/submit', [PublicController::class, 'contactsSubmit'])->name('contacts.submit');

//Language area//
Route::post('/locale/{locale}' , [PublicController::class, 'locale'])->name('locale');