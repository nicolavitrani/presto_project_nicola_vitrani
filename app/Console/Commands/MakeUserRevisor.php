<?php

namespace App\Console\Commands;

use App\Models\User;
use Illuminate\Console\Command;

class MakeUserRevisor extends Command
{
    /**
     * The name and signature of the console command.
     */
    protected $signature = 'presto:makeUserRevisor';

    /**
     * The console command description.
     */
    protected $description = 'Rendi un utente revisore';

    /**
     * Create a new command instance.
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     */
    public function handle()
    {
       $email = $this->ask("Inserisci l'email dell'utente che vuoi rendere revisore");
       $user = User::where('email', $email)->first();

       if(!$user){
           $this->error('Utente non trovato');
           return;
       }

       $user->is_revisor=true;
       $user->save();
       $this->info("L' Utente {$user->name} è ora un revisore.");

    }
}
