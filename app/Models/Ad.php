<?php

namespace App\Models;


use App\Models\User;
use App\Models\Category;
use Laravel\Scout\Searchable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Ad extends Model
{
    use Searchable;

    use HasFactory;

    use SoftDeletes;

    use Searchable;

    protected $fillable = [
        'title',
        'description',
        'price',
        'category_id',
        'user_id'
    ];

    public function category(){
       return $this->belongsTo(Category::class);
    }

    public function user(){
        return $this->belongsTo(User::class);
     }

    public function images(){
        return $this->hasMany(AdImage::class);
    }
    static public function ToBeRevisionedCount(){
        return Ad::where('is_accepted', null)->count();

    }

    static public function TrashedCount(){
        return Ad::withTrashed()->where('is_accepted', false)->count();

    }

    public function toSearchableArray()
    {
        $category = $this->category;

        $array = [
            'id' => $this->id,
            'title' => $this->title,
            'description' => $this->description,
            'category' => $category,
        ];

        return $array;
    }

}
