<?php

namespace App\Http\Controllers;

use App\Models\Ad;
use App\Models\Category;
use App\Mail\ContactMail;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

class PublicController extends Controller
{
    public function homepage() {
        $ads= Ad::orderBy('created_at', 'desc')->take(6)->get();
        $categories=Category::all();
        return view('homepage', compact('ads' , 'categories'));
    }

    public function adsByCategory($name, $category_id){
        $category = Category::find($category_id);
        $ads = $category->ads()->orderBy('created_at', 'desc')->paginate(4);
        return view('ads', compact('category', 'ads'));
    }

    public function contacts(){
        return view ('mail.send_email');
    }

    public function contactsSubmit(Request $request){
        $email = $request->input('email');
        $user = $request->input('user');
        $message = $request->input('message');
        $contact = compact('user', 'message');

       Mail::to($email)->send(new ContactMail($contact));

       return redirect(route('contacts'))->with('message', 'Email inviata correttamente.');
    }

    public function locale($locale){
        session()->put('locale', $locale);
        return redirect()->back();
    }
}
