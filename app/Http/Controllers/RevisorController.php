<?php

namespace App\Http\Controllers;

use App\Models\Ad;
use App\Models\AdImage;
use Illuminate\Http\Request;

class RevisorController extends Controller
{
    public function __construct(){
        $this->middleware('auth.revisor');
    }

    public function index(){
        $ad = Ad::where('is_accepted', null)
        ->orderBy('created_at', 'desc')
        ->first();

        return view('auth.revisor_home' , compact('ad'));
    }

    public function setAccepted($ad_id , $value){

        //Prende l'annuncio in base all'id che arriva.
        $ad = Ad::find($ad_id);
        $ad->is_accepted = $value;  //Valore true.
        $ad->save();
        return redirect(route('auth.revisor'));

    }

    public function accept($ad_id){
      return $this->setAccepted($ad_id, true);

    }

    public function reject($ad_id){
        //Di questo oggetto che ti creererò lanciami il setAccepted dove l'ad_id è false.
        $this->setAccepted($ad_id , false);
        $ad = Ad::find($ad_id);
        $ad->delete();
        return redirect()->back()->with('message', 'Annuncio eliminato correttamente');
    }


    public function trash(){
        //withTrashed = Con la colonna deleted_at valorizzata.
        $ads = Ad::withTrashed()->where('is_accepted', false)->get();
        return view('auth.revisor_trash', compact('ads'));
    }

    public function restore($ad_id){

        $ad = Ad::withTrashed()->where('id', $ad_id);  //Prendi l'annuncio che stiamo cliccando.
        $ad->restore();
        $this->setAccepted($ad_id, null);

        return redirect()->back()->with('message', 'Annuncio ripristinato correttamente');
    }

    public function delete($ad_id){

        $ad = Ad::withTrashed()->where('id',$ad_id);  //Prendi l'annuncio che stiamo cliccando.
        $ad_images = AdImage::where('ad_id',$ad_id);

        $ad_images->forceDelete();
        $ad->forceDelete();

        return redirect()->back()->with('message', 'Annuncio eliminato correttamente');

    }
}

