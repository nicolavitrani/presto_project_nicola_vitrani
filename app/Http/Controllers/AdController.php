<?php

namespace App\Http\Controllers;

use App\Models\Ad;
use App\Models\AdImage;
use App\Models\Category;
use App\Jobs\ResizeImage;
use Illuminate\Http\Request;
use App\Http\Requests\StoreRequest;
use App\Jobs\GoogleVisionLabelImage;
use App\Jobs\GoogleVisionRemoveFaces;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\Storage;
use App\Jobs\GoogleVisionSafeSearchImage;

class AdController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function __construct(){  //tutte le viste sono protette dal middleware e possono essere accessibili solo se si è loggati.
        $this->middleware('auth');

    }


    public function index()
    {
        // $ads = Ad::paginate(5);
        $ads = Ad::where('is_accepted', true)->orderBy('created_at', 'desc')->get();

        return view ('ad.index', compact('ads'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $uniqueSecret = $request->old(
            'uniqueSecret',
            base_convert(sha1(uniqid(mt_rand())), 16, 36)
        );
        return view('ad.form', compact('uniqueSecret'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreRequest $request)
    {
        $ad = Ad::create([
            'title'=>$request->title,
            'description'=>$request->description,
            'price'=>$request->price,
            'category_id'=>$request->category_id,
            'user_id' => Auth::user()->id,

            ]);

            $uniqueSecret = $request->input('uniqueSecret');

            $images = session()->get("images.{$uniqueSecret}", []);  // Leggere l'elenco delle immagini presenti nel box immagini
            $removedImages = session()->get("removedimages.{$uniqueSecret}", []);


            $images = array_diff($images, $removedImages);

            foreach ($images as $image) {
                $i = new AdImage();

                $fileName = basename($image);  //Cattura solo il nome dell'immagine
                $newFileName = "public/ads/{$ad->id}/{$fileName}";
                Storage::move($image, $newFileName); //Sposta l'immagine dalla cartella temporanea alla cartella che verrà creata

                // dispatch(new ResizeImage(
                //     $newFileName,
                //     300,
                //     150
                // ));

                // dispatch(new ResizeImage(
                //     $newFileName,
                //     400,
                //     300
                // ));

                $i->file = $newFileName;
                $i->ad_id = $ad->id;

                $i->save();

                GoogleVisionSafeSearchImage::withChain([
                    new GoogleVisionLabelImage($i->id),
                    new GoogleVisionRemoveFaces($i->id),
                    new ResizeImage($i->file, 300, 150),
                    new ResizeImage($i->file, 400, 300),
                    new ResizeImage($i->file, 400, 250),
                ])->dispatch($i->id);

                // dispatch(new GoogleVisionSafeSearchImage($i->id));
                // dispatch(new GoogleVisionLabelImage($i->id));
                // dispatch(new GoogleVisionRemoveFaces($i->id));
            }

            File::deleteDirectory(storage_path("/app/public/temp/{$uniqueSecret}"));



        return redirect('/creazione/annuncio')->with('message', 'Annuncio inserito correttamente.');
    }

    public function uploadImage(Request $request){

        $uniqueSecret = $request->input('uniqueSecret');

        $fileName = $request->file('file')->store("public/temp/{$uniqueSecret}");

        dispatch(new ResizeImage(
            $fileName,
            80,
            80
        ));

        session()->push("images.{$uniqueSecret}", $fileName);

        return response()->json(
            [
                'id' => $fileName,
            ]
        );

    }

    public function removeImage(Request $request){

        $uniqueSecret = $request->input('uniqueSecret');

        $fileName = $request->input('id');
        session()->push("removedimages.{$uniqueSecret}", $fileName);

        Storage::delete($fileName);

        return response()->json('ok');
    }

    public function getImages(Request $request){

        $uniqueSecret = $request->input('uniqueSecret');

        $images = session()->get("images.{$uniqueSecret}", []);
        $removedImages = session()->get("removedimages.{$uniqueSecret}", []);

        $images = array_diff($images, $removedImages);

        $data = [];

        foreach($images as $image){
            $data[] = [
                'id' => $image,
                'src' => AdImage::getUrlByFilePath($image, 80, 80)
            ];
        }

        return response()->json($data);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Ad  $ad
     * @return \Illuminate\Http\Response
     */
    public function show(Ad $ad)
    {
        return view('ad.dettaglioAd', compact('ad'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Ad  $ad
     * @return \Illuminate\Http\Response
     */
    public function edit(Ad $ad)
    {
        return view ('ad.edit', compact('ad'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Ad  $ad
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Ad $ad)
    {
        $ad->title = $request->title;
        $ad->description = $request->description;
        $ad->save();

        return redirect(route('ad.index', compact('ad')));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Ad  $ad
     * @return \Illuminate\Http\Response
     */
    public function destroy(Ad $ad)
    {
        $ad->delete();
        return redirect(route('ad.index'));
    }


    public function search(Request $request){
        $q = $request->input('q');

        $ads = Ad::search($q)->orderBy('created_at', 'desc')->get();

        return view('search.search_results', compact('q', 'ads')); //Qui viene passato gli annunci trovati e la richiesta dell'utente
    }

}
