<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title'=>'required|min:5|max:60',
            'description'=>'required|min:50|max:300'
        ];
    }

    public function message(){
        return [
            'title.required'=>'Inserisci titolo dell\'annuncio.',
            'title.min'=>'Il campo deve contenere un minimo di 5 caratteri.',
            'title.max'=>'Il campo deve contenere un massimo di 30 caratteri.',

            'description.required'=>'Inserisci descrizione dell\'annuncio.',
            'description.min'=>'Il campo deve contenere un minimo di 50 caratteri.',
            'description.max'=>'Il campo deve contenere un massimo di 300 caratteri.',
        ];
    }
}
